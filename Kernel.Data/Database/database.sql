-- Table: users
CREATE TABLE users (
    id_user int NOT NULL AUTO_INCREMENT,
    is_admin tinyint NOT NULL DEFAULT ((0)),
    username varchar(100) NOT NULL,
    password_hash longblob NOT NULL,
    password_salt longblob NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    deleted_at datetime NULL,
    CONSTRAINT users_pk PRIMARY KEY (id_user),
    INDEX users_idx_1 (is_admin ASC),
    INDEX users_idx_2 (id_company ASC),
    INDEX users_idx_3 (username ASC),
    INDEX users_idx_4 (created_at ASC),
    INDEX users_idx_5 (updated_at ASC),
    INDEX users_idx_6 (deleted_at ASC)
);