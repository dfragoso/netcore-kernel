﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Pomelo.EntityFrameworkCore.MySql.Storage;
using Kernel.CLI.UseCase;
using Kernel.Data;
using System;

namespace Kernel.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfigurationRoot _config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", false)
            .Build();

            var services = new ServiceCollection();

            services.AddDbContext<DbContextKernel>(options =>
               options.UseMySql(_config.GetConnectionString("CoreDataBase"), mySqlOptions => mySqlOptions
                     .ServerVersion(new ServerVersion(new Version(8, 0, 18), ServerType.MySql))
            ));

            var serviceProvider = services.BuildServiceProvider();
            var _context = serviceProvider.GetService<DbContextKernel>();

            var exit = false;
            while (!exit)
            {
                Console.WriteLine("CLI Kernel");
                Console.WriteLine();
                Console.WriteLine("1.- Create a User");
                Console.WriteLine("2.- User Change Password");
                Console.WriteLine("3.- Exit");
                Console.WriteLine();
                Console.Write("Choose an option: ");

                var choosed = Console.ReadLine();

                Console.Clear();

                switch (choosed)
                {
                    case "1":
                        new CreateUserCase(_context, _config).Execute().Wait();
                        break;
                    case "2":
                        new ChangeUserPasswordCase(_context, _config).Execute().Wait();
                        break;
                    case "3":
                        exit = true;
                        break;
                    default:
                        break;
                }
                Console.Clear();
            }
        }
    }
}
