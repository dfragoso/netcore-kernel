using Audit.Core;
using Audit.EntityFramework.Providers;
using Audit.MySql.Providers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Pomelo.EntityFrameworkCore.MySql.Storage;
using Kernel.API.Filters;
using Kernel.Data;
using Kernel.Domain.Interfaces;
using Kernel.Domain.Services;
using System;
using System.Text;
using Hangfire;
using Hangfire.LiteDB;
using System.IO;
using Hangfire.Dashboard;
using Hangfire.Dashboard.BasicAuthorization;
using Kernel.API.Configurations;

namespace Kernel.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            AuditConfig = Configuration.GetSection(nameof(AuditConfiguration)).Get<AuditConfiguration>();
            HangfireConfig = Configuration.GetSection(nameof(HangfireConfiguration)).Get<HangfireConfiguration>();
            JwtConfig = Configuration.GetSection(nameof(JwtConfiguration)).Get<JwtConfiguration>();
        }

        public IConfiguration Configuration { get; }
        public AuditConfiguration AuditConfig { get; }
        public HangfireConfiguration HangfireConfig { get; }
        public JwtConfiguration JwtConfig { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // Add Hangfire services.
            if(HangfireConfig.Use) services.AddHangfire(configuration => configuration
                .UseLiteDbStorage(Path.Combine("Storage","Hangfire","Hangfire.db"),new LiteDbStorageOptions())
            );

            // Add the processing server as IHostedService
            if (HangfireConfig.Use) services.AddHangfireServer();

            //Add the controllers
            services.AddControllers();

            //Add Filter to catch errors
            services.AddMvcCore(options =>
            {
                options.Filters.Add<GlobalExceptionFilterAttribute>();
                options.EnableEndpointRouting = false;
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
            .AddNewtonsoftJson(opt => opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);

            //Set MySQL Connection
            services.AddDbContext<DbContextKernel>(options =>
               options.UseMySql(Configuration.GetConnectionString("CoreDataBase"), mySqlOptions => mySqlOptions
                     .ServerVersion(new ServerVersion(new Version(8, 0, 18), ServerType.MySql))
            ));

            //Set JWT Configuration
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = JwtConfig.Issuer,
                        ValidAudience = JwtConfig.Issuer,
                        ClockSkew = TimeSpan.FromSeconds(10),
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtConfig.Key))
                    };
                });

            //Services Domain
            services.AddScoped<IUserService, UserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IBackgroundJobClient backgroundJobs)
        {
            //Use Audit in Entity only with Annotation
            Audit.EntityFramework.Configuration.Setup().ForAnyContext().UseOptIn();
            
            //Define Audit Configuration
            Audit.Core.Configuration.DataProvider = new MySqlDataProvider()
            {
                ConnectionString = Configuration.GetConnectionString("LogginDataBase"),
                TableName = AuditConfig.Table,
                IdColumnName = AuditConfig.IdColumn,
                JsonColumnName = AuditConfig.JsonColumn
            };

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            if (HangfireConfig.Use) app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new BasicAuthAuthorizationFilter(new BasicAuthAuthorizationFilterOptions
                {
                    RequireSsl = false,
                    SslRedirect = false,
                    LoginCaseSensitive = true,
                    Users = new []
                    {
                        new BasicAuthAuthorizationUser
                        {
                            Login = HangfireConfig.Username,
                            PasswordClear =  HangfireConfig.Password
                        }
                    }

                }) }
            });

            app.UseRouting();
            app.UseCors(builder => builder.WithOrigins("*").WithHeaders("*").WithMethods("*"));
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                if (HangfireConfig.Use) endpoints.MapHangfireDashboard();
            });

            loggerFactory.AddFile("Storage/Logs/Kernel-{Date}.log");
        }
    }
}
