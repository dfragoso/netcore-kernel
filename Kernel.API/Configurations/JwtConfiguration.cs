﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kernel.API.Configurations
{
    public class JwtConfiguration
    {
        [Required]
        public string Key { get; set; }

        [Required]
        public string Issuer { get; set; }
    }
}
