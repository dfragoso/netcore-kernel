﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kernel.API.Configurations
{
    public class AuditConfiguration
    {
        [Required]
        public string Table { get; set; }
        
        [Required]
        public string IdColumn { get; set; }
        
        [Required]
        public string JsonColumn { get; set; }
    }
}
