﻿using System.ComponentModel.DataAnnotations;

namespace Kernel.Domain.DTO.Security.Users
{
    public class UpdatePasswordViewModel
    {
        [Required]
        public int id_user { get; set; }

        [Required]
        public string password { get; set; }
    }
}
