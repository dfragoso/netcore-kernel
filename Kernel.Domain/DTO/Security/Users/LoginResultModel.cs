﻿namespace Kernel.Domain.DTO.Security.Users
{
    public class LoginResultModel
    {
        public string token { get; set; }
    }
}
