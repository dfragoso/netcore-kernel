﻿using System.ComponentModel.DataAnnotations;

namespace Kernel.Domain.DTO.Security.Users
{
    public class ChangePasswordViewModel
    {
        [Required]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "El username no debe de tener más de 100 caracteres, ni menos de 3 caracteres.")]
        public string username { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "La contraseña anterior no debe de tener más de 100 caracteres, ni menos de 6 caracteres.")]
        public string old_password { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "La contraseña nueva no debe de tener más de 100 caracteres, ni menos de 6 caracteres.")]
        public string new_password { get; set; }
    }
}
