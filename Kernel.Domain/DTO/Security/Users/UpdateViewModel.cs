﻿using System.ComponentModel.DataAnnotations;

namespace Kernel.Domain.DTO.Security.Users
{
    public class UpdateViewModel
    {
        [Required]
        public int id_user { get; set; }
    }
}
