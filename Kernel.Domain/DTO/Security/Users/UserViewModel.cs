﻿using System.ComponentModel.DataAnnotations;

namespace Kernel.Domain.DTO.Security.Users
{
    public class UserViewModel
    {
        [Required]
        public int id_user { get; set; }

        [Required]
        public string username { get; set; }
    }
}
