﻿using System.ComponentModel.DataAnnotations;

namespace Kernel.Domain.DTO.Security.Users
{
    public class LoginViewModel
    {
        [Required]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "El username no debe de tener más de 100 caracteres, ni menos de 3 caracteres.")]
        public string username { get; set; }
        [Required]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "La contraseña no debe de tener más de 100 caracteres, ni menos de 6 caracteres.")]
        public string password { get; set; }
    }
}
