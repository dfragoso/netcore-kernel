﻿using System;

namespace Kernel.Domain.Exceptions
{
    public class KernelAuthenticationException : KernelException
    {
        public KernelAuthenticationException() : base() { }
        public KernelAuthenticationException(Exception innerException) : base(innerException) { }
        public KernelAuthenticationException(string MSG) : base(MSG) { }
        public KernelAuthenticationException(Exception innerException, string MSG) : base(innerException, MSG) { }
    }
}
